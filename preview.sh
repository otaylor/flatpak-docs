#!/bin/sh

podman run --rm --network=host -v $(pwd):/antora:ro,z -v $(pwd)/nginx.conf:/etc/nginx/conf.d/default.conf:ro,z nginx
